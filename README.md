# AES single round

This repository implements _single_ **AES** algorithm round for education purposes.  

Consider this implementation as **beta** and don't hesitate to report any issues.  

This product comes without the warranty :)  

## TODO

Possible improvements aka **TODO**:  
1. assert all input values  
2. assert chunk size  
3. implement exceptions  
4. implement dynamic s-box generation  
5. implement interface for output data  
6. implement logging / verbosity in class  
7. implement / improve chunking methods inc. key extending  
8. optimize  