package com.aes.singleround;

import java.security.SecureRandom;

public class Transformations {
    // chunk matrix size is 4
    private static int chunkMatrixSize = 4;

    public static void main(String[] args) {

    }

    // transform given string into two dimensional array
    public byte[][] transformToChunk(String inputString) {
        byte[][] matrix = new byte[chunkMatrixSize][chunkMatrixSize];
        int cnt = 0;

        for (int row = 0; row < chunkMatrixSize; row++)
            for (int col = 0; col < chunkMatrixSize; col++) {
                byte character = (byte) inputString.charAt(cnt);
                matrix[row][col] = character;
                cnt++;
            }

        return matrix;
    }

    // print string from two dimensional array
    public String chunkToString(byte[][] chunk) {
        String message = "";

        for (int row = 0; row < chunkMatrixSize; row++)
            for (int col = 0; col < chunkMatrixSize; col++) {
                message += (char)chunk[row][col];
            }
        return message;
    }

    // generate random string form numbers an characters only
    // just simple implementation without sophisticated conditions
    public String randomString(int expectedLength) {

        SecureRandom random = new SecureRandom();
        String characters = "abcdefghijklmnopqrstuvwxyz";
        String charactersUpper = characters.toUpperCase();
        String number = "0123456789";
        String availableCharacters = characters + charactersUpper + number;

        StringBuilder stiringBuilder = new StringBuilder(expectedLength);
        for (int i = 0; i < expectedLength; i++) {

            int charPosition = random.nextInt(availableCharacters.length());
            char randomChar = availableCharacters.charAt(charPosition);

            stiringBuilder.append(randomChar);

        }

        return stiringBuilder.toString();
    }
}
