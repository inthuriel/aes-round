import com.aes.singleround.SingleRound;
import com.aes.singleround.Transformations;
import com.google.common.base.Splitter;

public class RunAesRound {
    /*
    This class is mock implementation of SingleRound class providing single AES round implementation
    This is an example how to use SingleRound class
     */

    // aes chunk size is 16 bytes (characters) / 128 bits
    private static int chunk_size = 16;

    public static void main(String[] args) {
        // round class instance
        SingleRound aes_round = new SingleRound();

        // class to provide various transformation functions
        Transformations transform = new Transformations();

        // as this is mock, we want just 1 128-bits chunk of data
        // to be sent to the round
        String test_text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, " +
                "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, " +
                "quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. " +
                "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. " +
                "Excepteur sint occaecat cupidatat non proident, " +
                "sunt in culpa qui officia deserunt mollit anim id est laborum.";

        // use Google Guava Splitter to split string into proper chunks
        Iterable<String> chunks = Splitter.fixedLength(chunk_size).split(test_text);
        // set chunk to send into round
        String chunk = chunks.iterator().next();

        // key for encryption
        // generate random  16 bytes (characters) / 128 bits string for one round
        String key = transform.randomString(chunk_size);

        // transform both key and text chunk into two dimensional arrays
        byte[][] bytesChunk = transform.transformToChunk(chunk);
        byte[][] bytesKey = transform.transformToChunk(key);

        // run round
        aes_round.round(bytesChunk, bytesKey);

        // print results as debug info
        System.out.printf("Original string chunk used: %s%n", chunk);
        System.out.printf("Key used in round: %s%n", transform.chunkToString(aes_round.keyChunk));
        System.out.printf("Encrypted string: %s%n", transform.chunkToString(aes_round.roundCipherChunk));

    }

}
